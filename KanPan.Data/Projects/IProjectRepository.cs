﻿using KanPan.Controllers.Projects.DataObjects;
using KanPan.Data.Common;
using KanPan.Data.User.DataObjects;
using KanPan.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanPan.Data.Projects
{
    public interface IProjectRepository : IAggregateRepository<Project>
    {
        Task<List<ProjectDo>> GetByEditor(string userId);
        Task<List<AccountDo>> GetEditors(Guid projectId);
        Task<ProjectDo> GetByEditorAndId(string userId, Guid projectId);
    }
}
