﻿using System;
using System.Collections.Generic;

namespace KanPan.Controllers.Projects.DataObjects
{
    public class ProjectDo
    {
        public ProjectDo()
        {
            Editors = new List<string>();
            BoardIdToPosition = new Dictionary<string, int>();
        }
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string OwnerId { get; set; }

        public List<string> Editors { get; set; }

        public Dictionary<string, int> BoardIdToPosition  { get; set; }
    }
}
