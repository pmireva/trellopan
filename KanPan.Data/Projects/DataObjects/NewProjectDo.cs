﻿using System;
using System.Collections.Generic;

namespace KanPan.Controllers.Projects.DataObjects
{
    public class NewProjectDo
    {
        public NewProjectDo()
        {
            Editors = new List<Guid>();
        }

        public string Title { get; set; }

        public List<Guid> Editors { get; set; }
    }
}
