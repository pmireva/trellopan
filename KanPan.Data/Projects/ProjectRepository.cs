﻿using KanPan.Common;
using KanPan.Controllers.Projects.DataObjects;
using KanPan.Data.Common;
using KanPan.Data.User;
using KanPan.Data.User.DataObjects;
using KanPan.Domain.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.Data.Projects
{
    public class ProjectRepository : AggregateRepository<Project>, IProjectRepository, IIndexedRepository
    {
        private readonly IAccountRepository _accountRepository;

        public ProjectRepository(
            IOptions<Settings> settings,
            EntityTracker entityTracker,
            IAccountRepository accountRepository) : base(settings, entityTracker)
        {
            _accountRepository = accountRepository;
        }

        public void EnsureIndexes()
        {
            dbContext.Set<Project>().Indexes.CreateOne(Builders<Project>.IndexKeys.Descending(s => s.Editors));
            dbContext.Set<Project>().Indexes.CreateOneAsync(new BsonDocument { { "Editors", 1 }, { "Id", 1 } });
        }

        public async Task<List<ProjectDo>> GetByEditor(string userId)
        {
            var projects = await this.dbContext.Set<Project>().Find(x => x.Editors.Contains(userId)).ToListAsync();

            return projects.Select(p => ConvertFromEntity(p)).ToList();
        }

        public async Task<ProjectDo> GetByEditorAndId(string userId, Guid projectId)
        {
            var project = await this.dbContext.Set<Project>().Find(x => x.Editors.Contains(userId) && x.Id == projectId).SingleAsync();

            return ConvertFromEntity(project);
        }

        public async Task<List<AccountDo>> GetEditors(Guid projectId)
        {
            var projectData = await this.dbContext.Set<Project>().Find(p => p.Id == projectId).SingleAsync();
            return await _accountRepository.Get(projectData.Editors);
         }

        private ProjectDo ConvertFromEntity(Project project)
        {
            return new ProjectDo
            {
                Id = project.Id,
                OwnerId = project.OwnerId,
                Editors = project.Editors,
                Title = project.Title,
                BoardIdToPosition = project.BoardIdToPosition
            };
        }
    }
}
