﻿namespace KanPan.Common
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
        public string SmtpClient;
        public string EmailSender;
        public string EmailSenderPass;
        public string EmailSenderName;
    }
}
