﻿using System;

namespace KanPan.Controllers.Stories.DataObjects
{
    public class DeleteStoryDo
    {
        public Guid Id { get; set; }

        public Guid BoardId { get; set; }
    }
}
