﻿using KanPan.Data.User.DataObjects;
using System;
using System.Collections.Generic;

namespace KanPan.Controllers.Stories.DataObjects
{
    public class StoryDo
    {
        public StoryDo()
        {
            Assigners = new List<AccountDo>();
        }

        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid BoardId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<AccountDo> Assigners { get; set; }
    }
}
