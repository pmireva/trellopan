﻿namespace KanPan.Controllers.Stories.DataObjects
{
    public class NewStoryDo
    {
        public NewStoryDo()
        {
        }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
