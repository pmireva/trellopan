﻿using System;
using System.Collections.Generic;

namespace KanPan.Controllers.Stories.DataObjects
{
    public class AssignedUsersDo
    {
        public List<string> Assigners { get; set; }
    }
}
