﻿using KanPan.Common;
using KanPan.Data.Common;
using KanPan.Domain.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanPan.Data.Stories
{
    public class StoryRepository : AggregateRepository<Story>, IIndexedRepository, IStoryRepository
    {
        public StoryRepository(
            IOptions<Settings> settings,
            EntityTracker entityTracker
           ) : base(settings, entityTracker)
        {
        }

        public void EnsureIndexes()
        {
            dbContext.Set<Story>().Indexes.CreateOne(Builders<Story>.IndexKeys.Descending(s => s.ProjectId));
            dbContext.Set<Story>().Indexes.CreateOne(Builders<Story>.IndexKeys.Descending(s => s.BoardId));
            dbContext.Set<Story>().Indexes.CreateOne(Builders<Story>.IndexKeys.Descending(s => s.AssignedTo));
        }

        public async Task<List<Story>> GetByProjectId(Guid projectId)
        {
            return await this.dbContext.Set<Story>().Find(b => b.ProjectId == projectId).ToListAsync();
        }

        public async Task RemoveByBoardId(Guid boardId)
        {
            await dbContext.Set<Story>().DeleteManyAsync(Builders<Story>.Filter.Eq(s => s.BoardId, boardId));
        }

        public async Task RemoveByProjectId(Guid projectId)
        {
            await dbContext.Set<Story>().DeleteManyAsync(Builders<Story>.Filter.Eq(s => s.ProjectId, projectId));
        }
        
        public async Task<List<Story>> GetAllAssignedStories()
        {
            return await dbContext.Set<Story>().Find(s => s.AssignedTo != null && s.AssignedTo.Count > 0).ToListAsync();
        }
    }
}
