﻿using KanPan.Data.Common;
using KanPan.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanPan.Data.Stories
{
    public interface IStoryRepository : IAggregateRepository<Story>, IIndexedRepository
    {
        Task<List<Story>> GetByProjectId(Guid projectId);
        Task<List<Story>> GetAllAssignedStories();

        Task RemoveByBoardId(Guid boardId);
        Task RemoveByProjectId(Guid project);
    }
}
