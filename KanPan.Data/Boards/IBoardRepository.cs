﻿using KanPan.Data.Boards.DataObjects;
using KanPan.Data.Common;
using KanPan.Domain.Models;
using System;
using System.Threading.Tasks;

namespace KanPan.Data.Boards
{
    public interface IBoardRepository : IAggregateRepository<Board>
    {
        Task<ProjectBoardsDo> GetByProject(string userId, Guid projectId);
        Task RemoveByProject(Guid projectId);
    }
}
