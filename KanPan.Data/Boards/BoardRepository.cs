﻿using System;
using System.Linq;
using System.Collections.Generic;
using KanPan.Common;
using KanPan.Data.Boards.DataObjects;
using KanPan.Data.Common;
using KanPan.Domain.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using KanPan.Controllers.Stories.DataObjects;
using KanPan.Data.Stories;
using KanPan.Data.User;
using KanPan.Data.Projects;
using KanPan.Data.User.DataObjects;

namespace KanPan.Data.Boards
{
    public class BoardRepository : AggregateRepository<Board>, IBoardRepository, IIndexedRepository
    {
        private readonly IStoryRepository _storyRepository;
        private readonly IProjectRepository _projectRepository;

        public BoardRepository(
            IOptions<Settings> settings,
            EntityTracker entityTracker,
            IStoryRepository storyRepository,
            IAccountRepository accountRepository,
            IProjectRepository projectRepository) : base(settings, entityTracker)
        {
            _projectRepository = projectRepository;
            _storyRepository = storyRepository;
        }

        public void EnsureIndexes()
        {
            dbContext.Set<Board>().Indexes.CreateOne(Builders<Board>.IndexKeys.Descending(s => s.ProjectId));
        }

        public async Task<ProjectBoardsDo> GetByProject(string userId, Guid projectId)
        {
            var project = await _projectRepository.GetByEditorAndId(userId, projectId);
            var boards = await this.dbContext.Set<Board>().Find(b => b.ProjectId == projectId).ToListAsync();
            var stories = await _storyRepository.GetByProjectId(projectId);
            var editors = (await _projectRepository.GetEditors(projectId));
            var googleIdToEditorMap = editors.ToDictionary(e => e.GoogleId);

            var boardIdToStoriesMap = stories.GroupBy(k => k.BoardId)
                .ToDictionary(s => s.Key,
                    m => m.Select(s => new StoryDo
                    {
                        Id = s.Id,
                        Title = s.Title,
                        Description = s.Description,
                        ProjectId = s.ProjectId,
                        BoardId = s.BoardId,
                        Assigners = s.AssignedTo != null ? s.AssignedTo.Where(a => googleIdToEditorMap.ContainsKey(a)).Select(a => googleIdToEditorMap[a]).ToList() : new List<AccountDo>()
                    })
                    .ToList());

            var boardsResult = boards.Select(board =>
                {
                    var boardStories = new List<StoryDo>();
                    if (boardIdToStoriesMap.ContainsKey(board.Id))
                    {
                        var storiesList = boardIdToStoriesMap[board.Id].AsEnumerable();
                        if (board.StoryIdToPosition != null)
                        {
                            storiesList = storiesList.OrderBy(s => board.StoryIdToPosition.ContainsKey(s.Id.ToString()) ? board.StoryIdToPosition[s.Id.ToString()] : 100);
                        }
                        boardStories = storiesList.ToList();
                    }
                    return new BoardDo
                    {
                        Id = board.Id,
                        Title = board.Title,
                        Stories = boardStories
                    };
                });

            if(project.BoardIdToPosition != null)
                boardsResult = boardsResult.OrderBy(b => project.BoardIdToPosition.ContainsKey(b.Id.ToString()) ? project.BoardIdToPosition[b.Id.ToString()] : 100);

            return new ProjectBoardsDo
            {
                Boards = boardsResult.ToList(),
                Editors = editors,
                Project = project
            };
        }

        public async Task RemoveByProject(Guid projectId)
        {
            await dbContext.Set<Board>().DeleteManyAsync(Builders<Board>.Filter.Eq(s => s.ProjectId, projectId));
        }
    }
}
