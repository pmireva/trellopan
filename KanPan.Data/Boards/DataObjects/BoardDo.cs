﻿using KanPan.Controllers.Stories.DataObjects;
using KanPan.Data.User.DataObjects;
using System;
using System.Collections.Generic;

namespace KanPan.Data.Boards.DataObjects
{
    public class BoardDo
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public List<StoryDo> Stories { get; set; }
    }
}
