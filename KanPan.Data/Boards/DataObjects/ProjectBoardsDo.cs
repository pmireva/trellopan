﻿using KanPan.Controllers.Projects.DataObjects;
using KanPan.Data.User.DataObjects;
using System.Collections.Generic;

namespace KanPan.Data.Boards.DataObjects
{
    public class ProjectBoardsDo
    {
        public ProjectBoardsDo()
        {
            Boards = new List<BoardDo>();
            Editors = new List<AccountDo>();
        }
        public List<BoardDo> Boards { get; set; }
        public List<AccountDo> Editors { get; set; }
        public ProjectDo Project { get; set; }
    }
}
