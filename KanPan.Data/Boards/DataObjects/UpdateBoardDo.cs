﻿using System;

namespace KanPan.Data.Boards.DataObjects
{
    public class UpdateBoardDo
    {
        public string Title { get; set; }
    }
}
