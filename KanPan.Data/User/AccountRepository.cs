﻿using KanPan.Common;
using KanPan.Data.Common;
using KanPan.Data.User.DataObjects;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.Data.User
{
    public class AccountRepository : AggregateRepository<Domain.Models.User>, IAccountRepository, IIndexedRepository
    {
        public AccountRepository(IOptions<Settings> settings, EntityTracker entityTracker) : base(settings, entityTracker)
        {
        }

        public void EnsureIndexes()
        {
            dbContext.Set<Domain.Models.User>().Indexes.CreateOne(Builders<Domain.Models.User>.IndexKeys.Descending(s => s.GoogleId));
            dbContext.Set<Domain.Models.User>().Indexes.CreateOne(Builders<Domain.Models.User>.IndexKeys.Descending(s => s.Email));
        }

        public async Task<List<AccountDo>> Get(List<string> googleIds)
        {
            var users = await dbContext.Set<Domain.Models.User>()
                .Find(u => googleIds.Contains(u.GoogleId))
                 .ToListAsync();

            return users.Select(u => new AccountDo
            {
                Email = u.Email,
                GoogleId = u.GoogleId,
                Initials = u.Initials,
                FirstName = u.FirstName,
                LastName = u.LastName
            }).ToList();
        }

        public async Task<AccountDo> Get(string googleId)
        {
            var user = await dbContext.Set<Domain.Models.User>()
                .Find(u => googleId == u.GoogleId)
                 .SingleOrDefaultAsync();

            AccountDo account = null;
            if (user != null)
            {
                account = new AccountDo
                {
                    Email = user.Email,
                    GoogleId = user.GoogleId,
                    Initials = user.Initials,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
            }

            return account;
        }

        public async Task<List<AccountDo>> Search(string term)
        {
            var users = await dbContext.Set<Domain.Models.User>()
               .Find(u => u.Email.Contains(term))
                .ToListAsync();

            return users.Select(u => new AccountDo
            {
                Email = u.Email,
                GoogleId = u.GoogleId,
                Initials = u.Initials,
                FirstName = u.FirstName,
                LastName = u.LastName
            }).ToList();
        }
    }
}
