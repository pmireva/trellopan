﻿using KanPan.Data.Common;
using KanPan.Data.User.DataObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanPan.Data.User
{
    public interface IAccountRepository : IAggregateRepository<Domain.Models.User>
    {
        Task<List<AccountDo>> Get(List<string> googleIds);
        Task<AccountDo> Get(string googleId);
        Task<List<AccountDo>> Search(string term);
    }
}
