﻿namespace KanPan.Data.User.DataObjects
{
    public class AccountDo
    {
        public string GoogleId { get; set; }

        public string Initials { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
