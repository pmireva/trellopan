﻿namespace KanPan.Controllers.DataObjects
{
    public class UserDo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
    }
}
