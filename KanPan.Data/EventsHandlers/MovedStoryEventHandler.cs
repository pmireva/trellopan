﻿using KanPan.Data.Boards;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using System.Threading.Tasks;

namespace KanPan.Data.EventsHandlers
{
    public class MovedStoryEventHandler : EventHandler<StoryMovedEvent>
    {
        private readonly IBoardRepository _boardRepository;

        public MovedStoryEventHandler(IBoardRepository boardRepository)
        {
            _boardRepository = boardRepository;
        }

        public override async Task Handle(StoryMovedEvent movedStoryData)
        {
            var oldBoard = await _boardRepository.Get(movedStoryData.OldBoardId);
            var updated = oldBoard.RemoveStory(movedStoryData.StoryId);
            if (updated)
                await _boardRepository.Update(oldBoard);

            var newStoryBoard = await _boardRepository.Get(movedStoryData.NewBoardId);
            newStoryBoard.UpdateStoriesPositions(movedStoryData.NewBoardStoriesPositions);
            await _boardRepository.Update(newStoryBoard);
        }
    }
}
