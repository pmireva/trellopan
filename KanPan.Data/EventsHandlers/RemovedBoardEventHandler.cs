﻿using KanPan.Data.Projects;
using KanPan.Data.Stories;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using System.Threading.Tasks;

namespace KanPan.Data.EventsHandlers
{
    public class RemovedBoardEventHandler : EventHandler<BoardRemovedEvent>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IStoryRepository _storyRepository;

        public RemovedBoardEventHandler(IProjectRepository projectRepository, IStoryRepository storyRepository)
        {
            _projectRepository = projectRepository;
            _storyRepository = storyRepository;
        }

        public override async Task Handle(BoardRemovedEvent removedBoardEvent)
        {
            await _storyRepository.RemoveByBoardId(removedBoardEvent.BoardId);

            var project = await _projectRepository.Get(removedBoardEvent.ProjectId);
            if (project.RemoveBoard(removedBoardEvent.BoardId))
            {
                await _projectRepository.Update(project);
            }
        }
    }
}
