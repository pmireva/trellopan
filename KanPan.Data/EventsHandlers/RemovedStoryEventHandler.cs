﻿using KanPan.Data.Boards;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using System.Threading.Tasks;

namespace KanPan.Data.EventsHandlers
{
    public class RemovedStoryEventHandler : EventHandler<StoryRemovedEvent>
    {
        private readonly IBoardRepository _boardRepository;

        public RemovedStoryEventHandler(IBoardRepository boardRepository)
        {
            _boardRepository = boardRepository;
        }

        public override async Task Handle(StoryRemovedEvent removedStoryEvent)
        {
            var board = await _boardRepository.Get(removedStoryEvent.BoardId);
            var updated = board.RemoveStory(removedStoryEvent.StoryId);
            if (updated)
                await _boardRepository.Update(board);
        }
    }
}
