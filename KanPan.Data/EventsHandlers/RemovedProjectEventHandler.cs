﻿using KanPan.Data.Boards;
using KanPan.Data.Stories;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using System.Threading.Tasks;

namespace KanPan.Data.EventsHandlers
{
    public class RemovedProjectEventHandler : EventHandler<ProjectRemovedEvent>
    {
        private readonly IBoardRepository _boardRepository;
        private readonly IStoryRepository _storyRepository;

        public RemovedProjectEventHandler(IStoryRepository storyRepository, IBoardRepository boardRepository)
        {
            _boardRepository = boardRepository;
            _storyRepository = storyRepository;
        }

        public override async Task Handle(ProjectRemovedEvent removedProjectEvent)
        {
            await _storyRepository.RemoveByProjectId(removedProjectEvent.ProjectId);
            await _boardRepository.RemoveByProject(removedProjectEvent.ProjectId);
        }
    }
}
