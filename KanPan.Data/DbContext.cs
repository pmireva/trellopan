﻿using KanPan.Common;
using KanPan.Data.Common;
using KanPan.Domain.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System.Collections.Generic;

namespace KanPan
{
    public class DbContext
    {
        private readonly IMongoDatabase _database;

        public DbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);

            var conventionPack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
            ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);

            ChangedEntities = new List<TEntity>();
         }
        
        public IMongoCollection<T> Set<T>()
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }

        public List<TEntity> ChangedEntities;
    }
}
