﻿namespace KanPan.Data.Common
{
    public interface IIndexedRepository
    {
        void EnsureIndexes();
    }
}
