﻿using KanPan.Common;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.Data.Common
{
    public abstract class AggregateRepository<T> : IAggregateRepository<T>
       where T : TEntity
    {
        protected readonly DbContext dbContext = null;
        private readonly EntityTracker _entityTracker;

        public AggregateRepository(IOptions<Settings> settings, EntityTracker entityTracker)
        {
            this.dbContext = new DbContext(settings);
            this._entityTracker = entityTracker;
        }

        public async Task<T> Get(Guid id)
        {
            return await dbContext.Set<T>().Find(x => x.Id == id).SingleAsync();
        }

        public async Task<T> Add(T item)
        {
            await dbContext.Set<T>().InsertOneAsync(item);
            this._entityTracker.Add((IEventEmitter)item);

            return item;
        }
        public async Task<T> Remove(Guid id)
        {
            var item = await dbContext.Set<T>().FindOneAndDeleteAsync(b => b.Id == id);
            this._entityTracker.Add((IEventEmitter)item);

            return item;
        }

        public async Task<ReplaceOneResult> Update(T item)
        {
            this._entityTracker.Add((IEventEmitter)item);

            return await dbContext.Set<T>().ReplaceOneAsync(n =>
                n.Id.Equals(item.Id),
                item,
                new UpdateOptions { IsUpsert = false });
        }

        public Task<DeleteResult> RemoveAll()
        {
            return dbContext.Set<T>().DeleteManyAsync(_ => true);
        }
    }
}
