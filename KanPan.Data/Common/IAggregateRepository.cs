﻿using KanPan.Domain.Models;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace KanPan.Data.Common
{
    public interface IAggregateRepository<T> where T : TEntity
    {
        Task<T> Get(Guid id);
        Task<T> Add(T item);
        Task<ReplaceOneResult> Update(T item);
        Task<T> Remove(Guid id);
        Task<DeleteResult> RemoveAll();
    }
}
