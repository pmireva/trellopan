﻿using KanPan.Domain.Core;
using System.Collections.Generic;

namespace KanPan.Data.Common
{
    public class EntityTracker
    {
        private List<IEventEmitter> ChangedEntities { get; }

        public EntityTracker()
        {
            ChangedEntities = new List<IEventEmitter>();
        }

        public List<IEventEmitter> GetAllChanged()
        {
            return ChangedEntities;
        }

        public void Add(IEventEmitter item)
        {
            ChangedEntities.Add(item);
        }

        public void RemoveAll()
        {
            ChangedEntities.RemoveAll(c => true);
        }
    }
}
