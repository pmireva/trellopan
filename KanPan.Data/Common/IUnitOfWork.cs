﻿using KanPan.Domain.Core;

namespace KanPan.Data.Common
{
    public interface IUnitOfWork
    {
        void DispatchEvents();
    }
}
