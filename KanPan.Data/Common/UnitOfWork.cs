﻿using KanPan.Domain.Core;
using System.Collections.Generic;
using System.Linq;

namespace KanPan.Data.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private EntityTracker entityTracker;
        private IEnumerable<IEventHandler> eventHandlers;

        public UnitOfWork(EntityTracker entityTracker, IEnumerable<IEventHandler> eventHandlers)
        {
            this.entityTracker = entityTracker;
            this.eventHandlers = eventHandlers;
        }

        public void DispatchEvents()
        {
            var allChangedEntities = entityTracker.GetAllChanged();
            var allChangedEntityIdToEntity = allChangedEntities.ToDictionary(e => e.Id);
            foreach (var entityId in allChangedEntityIdToEntity.Keys)
            {
                var events = allChangedEntityIdToEntity[entityId].Events;
                foreach (var domainEvent in events)
                {
                    foreach (var eventHandler in this.eventHandlers)
                    {
                        eventHandler.Handle(domainEvent);
                    }
                }
                allChangedEntityIdToEntity[entityId].Events.Clear();
            }
            entityTracker.RemoveAll();
        }
    }
}
