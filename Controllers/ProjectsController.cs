﻿using KanPan.Common;
using KanPan.Controllers.Projects.DataObjects;
using KanPan.Data.Common;
using KanPan.Data.Projects;
using KanPan.Data.User;
using KanPan.Data.User.DataObjects;
using KanPan.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KanPan.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IUnitOfWork _unitOfWork;

        private readonly UserContext _userContext;

        public ProjectsController(
            IProjectRepository projectRepository,
            IAccountRepository accountRepository,
            IUnitOfWork unitOfWork,
            UserContext userContext)
        {
            _projectRepository = projectRepository;
            _accountRepository = accountRepository;
            _unitOfWork = unitOfWork;
            _userContext = userContext;
        }

        [HttpGet]
        public Task<List<ProjectDo>> GetUserProjects()
        {
            return _projectRepository.GetByEditor(_userContext.UserId);
        }
        
        [HttpGet("{projectId}")]
        public Task<ProjectDo> GetUserProject(Guid projectId)
        {
            return _projectRepository.GetByEditorAndId(_userContext.UserId, projectId);
        }

        [HttpPost("new")]
        public Task<List<ProjectDo>> PostNew([FromBody]NewProjectDo project)
        {
            //editors are to be added
            var newProject = new Project(project.Title, _userContext.UserId);

            _projectRepository.Add(newProject);

            return _projectRepository.GetByEditor(_userContext.UserId);
        }
 
        [HttpPost("{projectId}")]
        public async Task<OkResult> Update(Guid projectId, [FromBody]ProjectDo projectDo)
        {
            var project = await _projectRepository.Get(projectId);

            project.Update(projectDo.Title, projectDo.OwnerId, projectDo.Editors);

            await _projectRepository.Update(project);

            return Ok();
        }

        [HttpGet("{projectId}/editors")]
        public async Task<List<AccountDo>> GetEditors(Guid projectId)
        {
            var editors = await _projectRepository.GetEditors(projectId);

            return editors;
        }

        [HttpPost("{projectId}/editors/add/{editorId}")]
        public async Task<AccountDo> AddEditor(Guid projectId, string editorId)
        {
            var project = await _projectRepository.Get(projectId);

            var existingAccount = await _accountRepository.Get(editorId);
            if (existingAccount == null)
            {
                throw new ArgumentException("This account hasn't been registered yet.");
            }
            project.AddEditor(editorId);

            await _projectRepository.Update(project);

            return new AccountDo
            {
                Email = existingAccount.Email,
                Initials = existingAccount.Initials,
                GoogleId = existingAccount.GoogleId
            };
        }

        [HttpPost("{projectId}/editors/remove/{editorId}")]
        public async Task<OkResult> RemoveEditor(Guid projectId, string editorId)
        {
            var project = await _projectRepository.Get(projectId);

            project.RemoveEditor(editorId);

            await _projectRepository.Update(project);

            return Ok();
        }

        [HttpPost("boardsPositions/{id}")]
        public async Task<OkResult> UpdateBoardsPositionsAsync(Guid id, [FromBody]List<Guid> boardIdToPosition)
        {
            var project = await _projectRepository.Get(id);

            project.UpdateBoardsPositions(boardIdToPosition);

            await _projectRepository.Update(project);

            return Ok();
        }

        [HttpPost("delete/{id}")]
        public async Task<OkResult> Delete(Guid id)
        {
            var project = await _projectRepository.Remove(id);
            project.MarkAsRemoved();

            _unitOfWork.DispatchEvents();

            return Ok();
        }
    }
}
