﻿using KanPan.Common;
using KanPan.Data.Projects;
using KanPan.Data.User;
using KanPan.Data.User.DataObjects;
using KanPan.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly UserContext _userContext;

        public AccountsController(
            IAccountRepository accountRepository,
            IProjectRepository projectRepository,
            UserContext userContext)
        {
            _accountRepository = accountRepository;
            _projectRepository = projectRepository;
            _userContext = userContext;
        }

        [HttpGet("")]
        public async Task<AccountDo> GetAccount()
        {
            return await _accountRepository.Get(_userContext.UserId);
        }
        
        [HttpGet("{projectId}/search")]
        public async Task<List<AccountDo>> SearchAccounts(Guid projectId, string term = null)
        {
            var alreadyAdded = (await _projectRepository.GetEditors(projectId))
                .Select(a => a.GoogleId)
                .ToList();
            return (await _accountRepository.Search(term)).Where(a => !alreadyAdded.Contains(a.GoogleId)).ToList();
        }

        [HttpPost("new")]
        public async Task<AccountDo> Register()
        {
            var existingAccount = await _accountRepository.Get(_userContext.UserId);
            User newUser = null;
            if (existingAccount == null)
            {
                var initials = string.Format("{0}{1}", _userContext.FirstName.Substring(0, 1), _userContext.LastName.Substring(0, 1));
                newUser = new User(_userContext.UserId, initials, _userContext.Email, _userContext.FirstName, _userContext.LastName);
                await _accountRepository.Add(newUser);
            }

            return new AccountDo
            {
                Initials = newUser.Initials,
                Email = newUser.Email,
                GoogleId = newUser.GoogleId,
                FirstName = newUser.FirstName,
                LastName = newUser.LastName
            };
        }
    }
}
