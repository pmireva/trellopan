﻿using KanPan.Common;
using KanPan.Controllers.Stories.DataObjects;
using KanPan.Data.Boards;
using KanPan.Data.Common;
using KanPan.Data.Projects;
using KanPan.Data.Stories;
using KanPan.Data.User.DataObjects;
using KanPan.Domain.Core;
using KanPan.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.Controllers
{
    [Authorize]
    [Route("api/[controller]/{projectId}/{boardId}")]
    public class StoriesController :  ApiHubController<Broadcaster>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IStoryRepository _storyRepository;
        private readonly IBoardRepository _boardRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserContext _userContext;

        public StoriesController(
            IConnectionManager signalRConnectionManager,
            IProjectRepository projectRepository,
            IBoardRepository boardRepository,
            IStoryRepository storyRepository,
            IUnitOfWork unitOfWork,
            UserContext userContext
            ) : base(signalRConnectionManager)
        {
            _projectRepository = projectRepository;
            _storyRepository = storyRepository;
            _boardRepository = boardRepository;
            _unitOfWork = unitOfWork;
            _userContext = userContext;
        }

        [HttpPost("new")]
        public async Task<OkResult> PostNewAsync(Guid projectId, Guid boardId, [FromBody]NewStoryDo newStory)
        {
            var story = new Story(
                _userContext.UserId,
                projectId, boardId, 
                newStory.Title, 
                newStory.Description);

            await _storyRepository.Add(story);
            var board = await _boardRepository.Get(boardId);

            Clients.Group(projectId.ToString()).AddNewStory(
                new StoryDo
                {
                    Id = story.Id,
                    ProjectId = projectId,
                    BoardId = boardId,
                    Description = story.Description,
                    Title = story.Title
                });

            return Ok();
        }

        [HttpPost("update/{storyId}")]
        public async Task<OkResult> UpdateAsync(Guid projectId, Guid boardId, Guid storyId, [FromBody]StoryDo storyDo)
        {
            var story = await _storyRepository.Get(storyId);

            story.UpdateContent(storyDo.Title, storyDo.Description);

            await _storyRepository.Update(story);
            var editors = (await _projectRepository.GetEditors(projectId));
            var googleIdToEditorMap = editors.ToDictionary(e => e.GoogleId);

            await Clients.Group(projectId.ToString())
                .UpdateStory(new StoryDo
                {
                    Id = storyId,
                    ProjectId = projectId,
                    BoardId = boardId,
                    Title = story.Title,
                    Description = story.Description,
                    Assigners = story.AssignedTo != null ? story.AssignedTo.Where(a => googleIdToEditorMap.ContainsKey(a)).Select(a => googleIdToEditorMap[a]).ToList() : new List<AccountDo>()
                });

            return Ok();
        }

        [HttpPost("update/{storyId}/assigners")]
        public async Task<OkResult> UpdateAssigners(Guid projectId, Guid boardId, Guid storyId, [FromBody]AssignedUsersDo request)
        {
            var story = await _storyRepository.Get(storyId);

            story.UpdateAssigners(request.Assigners);

            await _storyRepository.Update(story);

            //to do broadcast change

            return Ok();
        }

        [HttpPost("update/{storyId}/move")]
        public async Task<OkResult> MoveToBoard(Guid projectId, Guid boardId, Guid storyId, [FromBody]List<Guid> storyIdToPosition)
        {
            var story = await _storyRepository.Get(storyId);

            story.MoveToBoard(boardId, storyIdToPosition);
            await _storyRepository.Update(story);
            
            _unitOfWork.DispatchEvents();

            //to do broadcast change

            return Ok();
        }

        [HttpPost("delete/{storyId}")]
        public async Task<OkResult> Delete(Guid projectId, Guid boardId, Guid storyId)
        {
            var story = await _storyRepository.Remove(storyId);
            story.MarkAsRemoved();

            _unitOfWork.DispatchEvents();

            Clients.Group(projectId.ToString()).DeleteStory(
                new DeleteStoryDo
                {
                    Id = storyId,
                    BoardId = boardId
                });

            return Ok();
        }
    }
}
