﻿using KanPan.Common;
using KanPan.Data.Boards;
using KanPan.Data.Boards.DataObjects;
using KanPan.Data.Projects;
using KanPan.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using KanPan.Data.Stories;
using System.Collections.Generic;
using KanPan.Controllers.Stories.DataObjects;
using KanPan.Data.User;
using KanPan.Data.Common;

namespace KanPan.Controllers
{
    [Authorize]
    [Route("api/[controller]/{projectId}")]
    public class BoardsController : ApiHubController<Broadcaster>
    {
        private readonly IBoardRepository _boardRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IStoryRepository _storyRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserContext _userContext;

        public BoardsController(
            IConnectionManager signalRConnectionManager,
            IBoardRepository boardRepository,
            IProjectRepository projectRepository,
            IStoryRepository storyRepository,
            IAccountRepository accountRepository,
            IUnitOfWork unitOfWork,
            UserContext userContext
            ) : base(signalRConnectionManager)
        {
            _boardRepository = boardRepository;
            _projectRepository = projectRepository;
            _storyRepository = storyRepository;
            _accountRepository = accountRepository;
            _userContext = userContext;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ProjectBoardsDo> GetProjectBoardsData(Guid projectId)
        {
            return await _boardRepository.GetByProject(_userContext.UserId, projectId);            
        }

        [HttpPost("new")]
        public OkResult PostNewAsync(Guid projectId, [FromBody]NewBoardDo board)
        {
            var newBoard = new Board(projectId, board.Title);
            //TODO check if userId has permissions on this projects

            _boardRepository.Add(newBoard);

            Clients.Group(projectId.ToString())
                .AddNewBoard(new BoardDo
                {
                    Title = newBoard.Title,
                    Id = newBoard.Id,
                    Stories = new List<StoryDo>()
                });

            return Ok();
        }

        [HttpPost("update/{boardId}")]
        public async Task<OkResult> UpdateAsync(Guid projectId, Guid boardId, [FromBody]UpdateBoardDo boardDo)
        {
            var board = await _boardRepository.Get(boardId);

            board.Update(boardDo.Title);

            await _boardRepository.Update(board);

            Clients.Group(projectId.ToString())
                .UpdateBoard(new BoardDo
                {
                    Title = board.Title,
                    Id = board.Id
                });

            return Ok();
        }

        [HttpPost("delete/{boardId}")]
        public async Task<OkResult> Delete(Guid projectId, Guid boardId)
        {
            var board = await _boardRepository.Remove(boardId);
            board.MarkAsRemoved();

            Clients.Group(projectId.ToString()).DeleteBoard(boardId);

            _unitOfWork.DispatchEvents();

            return Ok();
        }

        [HttpPost("{boardId}/storiesPositions")]
        public async Task<OkResult> UpdateBoardsPositionsAsync(Guid projectId, Guid boardId, [FromBody]List<Guid> storyIdToPosition)
        {
            var board = await _boardRepository.Get(boardId);

            board.UpdateStoriesPositions(storyIdToPosition);

            await _boardRepository.Update(board);

            return Ok();
        }
    }
}
