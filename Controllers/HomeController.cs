using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanPan
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
       
        public IActionResult Index()
        {
            _logger.LogError("HomeController: Helloworld from Index");
            return View();
        }
    }
}
