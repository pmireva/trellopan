﻿using Newtonsoft.Json;
using System;

namespace KanPan.Common
{
    public class GoogleApiTokenInfo
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "given_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "family_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "sub")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "exp")]
        public string ExpInfo { get; set; }

        public TimeSpan ExpiresIn
        {

            get
            {
                int expiresInConverted = 0;
                int.TryParse(ExpInfo, out expiresInConverted);

                return TimeSpan.FromSeconds(expiresInConverted);
            }
        }
    }
}
