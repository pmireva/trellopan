﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace KanPan.Common
{
    public class UserContext
    {

        public string UserId { get; private set; }
        public string UserName { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public UserContext(IHttpContextAccessor contextAccessor)
        {
            if (contextAccessor.HttpContext.User == null || contextAccessor.HttpContext.User.Claims.Count() == 0)
                return;

            var claims = contextAccessor.HttpContext.User.Claims.Select(c => new JProperty(c.Type, c.Value));
            var userInfo = JsonConvert.DeserializeObject<GoogleApiTokenInfo>(new JObject(claims).ToString());

            UserId = userInfo?.UserId;
            UserName = userInfo?.Name;
            Email = userInfo?.Email;
            FirstName = userInfo?.FirstName;
            LastName = userInfo?.LastName;
        }
    }
}
