﻿using KanPan.Controllers.Stories.DataObjects;
using KanPan.Data.Boards.DataObjects;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace KanPan.Common
{
    public class Broadcaster : Hub<IBroadcaster>
    {
        public override Task OnConnected()
        {
            // Set connection id for just connected client only
            return Clients.Client(Context.ConnectionId).SetConnectionId(Context.ConnectionId);
        }

        // Server side methods called from client
        public Task Subscribe(Guid projectId)
        {
            return Groups.Add(Context.ConnectionId, projectId.ToString());
        }

        public Task Unsubscribe(Guid projectId)
        {
            return Groups.Remove(Context.ConnectionId, projectId.ToString());
        }
    }

    public interface IBroadcaster
    {
        Task SetConnectionId(string connectionId);

        Task UpdateBoards(BoardDo board);
        Task AddNewBoard(BoardDo board);
        Task DeleteBoard(Guid boardId);

        Task AddNewStory(StoryDo story);
        Task UpdateStory(StoryDo story);
        Task DeleteStory(DeleteStoryDo storyId);
    }
}
