﻿namespace KanPan.Common
{
    public class Constants
    {
        public const string TOKEN_TYPE = "Bearer";
        public const string VALID_ISSUER = "accounts.google.com";
        public const string CLIENT_ID = "64608429341-ap8dt8snj1ulpph4rua0kjpvojoqnijh.apps.googleusercontent.com";
        public const string AUTORIZATION_AUTHORITY = "https://accounts.google.com";
    }
}
