﻿using Hangfire;
using Hangfire.Mongo;
using KanPan.Common;
using KanPan.Data.Boards;
using KanPan.Data.Common;
using KanPan.Data.EventsHandlers;
using KanPan.Data.Projects;
using KanPan.Data.Stories;
using KanPan.Data.User;
using KanPan.Domain.Core;
using KanPan.EmailManager;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using System.IdentityModel.Tokens.Jwt;
using System.IO;

namespace KanPan
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json"), optional: false, reloadOnChange: true)
                .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.{env.EnvironmentName}.json"), optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                   .MinimumLevel.Error()
                   .WriteTo.RollingFile(Path.Combine(Directory.GetCurrentDirectory(), "logs/{Date}.txt"))
                   .CreateLogger();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy(Constants.TOKEN_TYPE, new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
            
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            string connectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
            string database = Configuration.GetSection("MongoConnection:Database").Value;

            services.Configure<Settings>(options =>
            {
                options.ConnectionString = connectionString;
                options.Database = database;
                options.EmailSender = Configuration.GetSection("EmailSender:From").Value;
                options.EmailSenderPass = Configuration.GetSection("EmailSender:Pass").Value;
                options.EmailSenderName = Configuration.GetSection("EmailSender:Name").Value;
            });

            services.AddSwaggerGen(c =>
            {
                // the Api key is a generated token with prefix of 'Bearer '
                c.AddSecurityDefinition(Constants.TOKEN_TYPE, new ApiKeyScheme() { In = "header", Description = "Please insert JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.SwaggerDoc("v1", new Info { Title = "KanPan", Version = "v1" });
            });

            services.AddSignalR(options => options.Hubs.EnableDetailedErrors = true);
            services.AddHangfire(hf => hf.UseMongoStorage(connectionString, database));

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<EmailSender>();

            services.AddTransient<IProjectRepository, ProjectRepository>();
            services.AddTransient<IIndexedRepository, ProjectRepository>();

            services.AddTransient<IBoardRepository, BoardRepository>();
            services.AddTransient<IIndexedRepository, BoardRepository>();

            services.AddTransient<IStoryRepository, StoryRepository>();
            services.AddTransient<IIndexedRepository, StoryRepository>();

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IIndexedRepository, AccountRepository>();
            
            services.AddScoped<UserContext>();
            services.AddScoped<EntityTracker>();
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IEventHandler, MovedStoryEventHandler>();
            services.AddTransient<IEventHandler, RemovedStoryEventHandler>();
            services.AddTransient<IEventHandler, RemovedBoardEventHandler>();
            services.AddTransient<IEventHandler, RemovedProjectEventHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            #region logger

            loggerFactory.AddSerilog();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            #endregion

            #region static files

            app.UseStaticFiles();
            
            #endregion

            #region Handle Exception

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Use(async (context, next) =>
                {
                    var error = context.Features[typeof(IExceptionHandlerFeature)] as IExceptionHandlerFeature;

                    //when authorization has failed, should retrun a json message to client
                    if (error != null && error.Error is SecurityTokenExpiredException)
                    {
                        context.Response.StatusCode = 401;
                        context.Response.ContentType = "application/json";

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new RequestResult
                        {
                            State = RequestState.NotAuth,
                            Msg = "token expired"
                        }));
                    }
                    //when orther error, retrun a error message json to client
                    else if (error != null && error.Error != null)
                    {
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new RequestResult
                        {
                            State = RequestState.Failed,
                            Msg = error.Error.Message
                        }));
                    }
                    //when no error, do next.
                    else await next();
                });
            });

            #endregion

            #region UseJwtBearerAuthentication

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseJwtBearerAuthentication(new JwtBearerOptions()
            {
                Authority = Constants.AUTORIZATION_AUTHORITY,
                Audience = Constants.CLIENT_ID,
                RequireHttpsMetadata = false,
                AutomaticAuthenticate = true,
                AutomaticChallenge = false,
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = Constants.VALID_ISSUER
                }
            });

            #endregion

            #region Swagger

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            #endregion

            #region SignalR

            app.UseSignalR();

            #endregion

            #region Hangfire

            app.UseHangfireServer();
            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate(() => new EmailSender().SendMails(), Cron.Weekly(System.DayOfWeek.Monday, 10));

            #endregion

            #region routes

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
            });

            #endregion


            #region

            if (env.IsDevelopment())
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var settings = serviceScope.ServiceProvider.GetService<IOptions<Settings>>();
                var repositories = serviceScope.ServiceProvider.GetServices<IIndexedRepository>();
                foreach (var repo in repositories)
                {
                    repo.EnsureIndexes();
                }
            }

            #endregion
        }
    }
}
