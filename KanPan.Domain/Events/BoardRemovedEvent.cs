﻿using KanPan.Domain.Core;
using System;

namespace KanPan.Domain.Models
{
    public class BoardRemovedEvent : IDomainEvent
    {
        public Guid ProjectId { get; set; }
        public Guid BoardId { get; set; }
    }
}
