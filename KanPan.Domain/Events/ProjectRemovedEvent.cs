﻿using KanPan.Domain.Core;
using System;

namespace KanPan.Domain.Models
{
    public class ProjectRemovedEvent : IDomainEvent
    {
        public Guid ProjectId { get; set; }
    }
}
