﻿using KanPan.Domain.Core;
using System;
using System.Collections.Generic;

namespace KanPan.Domain.Models
{
    public class StoryMovedEvent : IDomainEvent
    {
        public Guid StoryId { get; set; }
        public Guid NewBoardId { get; set; }
        public Guid OldBoardId { get; set; }
        public List<Guid> NewBoardStoriesPositions { get; set; }
    }
}
