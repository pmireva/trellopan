﻿using KanPan.Domain.Core;
using System;

namespace KanPan.Domain.Models
{
    public class StoryRemovedEvent : IDomainEvent
    {
        public Guid StoryId { get; set; }
        public Guid BoardId { get; set; }
    }
}
