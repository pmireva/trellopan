﻿using System.Threading.Tasks;

namespace KanPan.Domain.Core
{
    public abstract class EventHandler<TEvent> : IEventHandler where TEvent : IDomainEvent
    {
        public async Task Handle(IDomainEvent e)
        {
            if (e is TEvent)
            {
                await this.Handle((TEvent)e);
            }
        }

        public abstract Task Handle(TEvent e);
    }
}
