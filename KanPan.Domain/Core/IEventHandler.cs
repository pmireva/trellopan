﻿using System.Threading.Tasks;

namespace KanPan.Domain.Core
{
    public interface IEventHandler
    {
        Task Handle(IDomainEvent domainEvent);
    }
}
