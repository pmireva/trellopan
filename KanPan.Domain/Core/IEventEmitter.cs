﻿using System;
using System.Collections.Generic;

namespace KanPan.Domain.Core
{
    public interface IEventEmitter
    {
        Guid Id { get; set; }

        ICollection<IDomainEvent> Events { get; set; }
    }
}
