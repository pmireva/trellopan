﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace KanPan.Domain.Models
{
    public class TEntity
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}
