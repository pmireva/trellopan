﻿using System;

namespace KanPan.Domain.Models
{
    public class Sequence
    {
        public string Name { get; set; }
        public int LastUsedId { get; set; }
    }
}
