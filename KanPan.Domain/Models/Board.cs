﻿using KanPan.Domain.Core;
using System;
using System.Collections.Generic;

namespace KanPan.Domain.Models
{
    public class Board : TEntity, IEventEmitter
    {
        public Board()
        {
            ((IEventEmitter)this).Events = new List<IDomainEvent>();
        }

        public Board(Guid projectId, string title) : this()
        {
            CreatedOn = DateTime.UtcNow;
            Title = title;
            ProjectId = projectId;
        }
        
        public Guid ProjectId { get; private set; }

        public string Title { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public DateTime UpdatedOn { get; private set; }

        public Dictionary<string, int> StoryIdToPosition { get; private set; }

        ICollection<IDomainEvent> IEventEmitter.Events { get; set; }

        public void Update(string title)
        {
            this.Title = title;
            this.UpdatedOn = DateTime.UtcNow;
        }

        public void UpdateStoriesPositions(List<Guid> orderedStoriesIds)
        {
            var storydIdToPosition = new Dictionary<string, int>();
            orderedStoriesIds.ForEach(b =>
            {
                int index = orderedStoriesIds.IndexOf(b);
                storydIdToPosition.Add(b.ToString(), index);
            });

            UpdatedOn = DateTime.UtcNow;

            StoryIdToPosition = storydIdToPosition;
        }

        public bool RemoveStory(Guid storyId)
        {
            if (StoryIdToPosition != null)
            {
                StoryIdToPosition.Remove(storyId.ToString());
                return true;
            }
            return false;
        }

        public void MarkAsRemoved()
        {
            ((IEventEmitter)this).Events.Add(new BoardRemovedEvent
            {
                BoardId = Id,
                ProjectId = this.ProjectId
            });
        }
    }
}
