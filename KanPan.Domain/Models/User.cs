﻿using System.Collections.Generic;
using KanPan.Domain.Core;

namespace KanPan.Domain.Models
{
    public class User : TEntity, IEventEmitter
    {
        public User()
        {
            this.Events = new List<IDomainEvent>();
        }

        public User(
            string googleId,
            string initials,
            string email,
            string firstName,
            string lastName)
        {
            this.GoogleId = googleId;
            this.Initials = initials;
            this.Email = email;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public string GoogleId { get; private set; }

        public string Initials { get; private set; }

        public string Email { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public ICollection<IDomainEvent> Events { get; set; }
    }
}
