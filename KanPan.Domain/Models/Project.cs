﻿using KanPan.Domain.Core;
using System;
using System.Collections.Generic;

namespace KanPan.Domain.Models
{
    public class Project : TEntity, IEventEmitter
    {
        public Project()
        {
            Editors = new List<string>();
            ((IEventEmitter)this).Events = new List<IDomainEvent>();
        }

        public Project(string title, string ownerId) : this()
        {
            CreatedOn = DateTime.UtcNow;

            Title = title;
            OwnerId = ownerId;

            AddEditor(ownerId);
        }

        public string Title { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public DateTime UpdatedOn { get; private set; }

        public string OwnerId { get; private set; }

        public List<string> Editors { get; private set; }

        public Dictionary<string, int> BoardIdToPosition { get; private set; }

        ICollection<IDomainEvent> IEventEmitter.Events { get; set; }
        
        public void Rename(string title)
        {
            Title = title;
        }
        
        public void AddEditor(string editorGoogleId)
        {
            if (!Editors.Contains(editorGoogleId))
                Editors.Add(editorGoogleId);
        }

        public void RemoveEditor(string editorGoogleId)
        {
            if (Editors.Contains(editorGoogleId))
                Editors.Remove(editorGoogleId);

            UpdatedOn = DateTime.UtcNow;
        }

        public void Update(string title, string ownerId, List<string> editors)
        {
            Title = title;
            OwnerId = ownerId;
            Editors = editors;

            UpdatedOn = DateTime.UtcNow;

        }

        public void UpdateBoardsPositions(List<Guid> orderedBoardsIds)
        {
            var boardIdToPosition = new Dictionary<string, int>();
            orderedBoardsIds.ForEach(b =>
            {
                int index = orderedBoardsIds.IndexOf(b);
                boardIdToPosition.Add(b.ToString(), index);
            });

            UpdatedOn = DateTime.UtcNow;

            BoardIdToPosition = boardIdToPosition;
        }

        public bool RemoveBoard(Guid boardId)
        {
            if (BoardIdToPosition.ContainsKey(boardId.ToString()))
            {
                BoardIdToPosition.Remove(boardId.ToString());
                UpdatedOn = DateTime.UtcNow;
                return true;
            }
            return false;
        }

        public void MarkAsRemoved()
        {
            ((IEventEmitter)this).Events.Add(new ProjectRemovedEvent
            {
                ProjectId = this.Id
            });
        }
    }
}
