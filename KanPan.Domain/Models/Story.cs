﻿using KanPan.Domain.Core;
using System;
using System.Collections.Generic;

namespace KanPan.Domain.Models
{
    public class Story : TEntity, IEventEmitter
    {
        public Story()
        {
            this.AssignedTo = new List<string>();
            ((IEventEmitter)this).Events = new List<IDomainEvent>();
        }

        public Story(
            string creatorId,
            Guid projectId,
            Guid boardId,
            string title,
            string description)
        {
            this.CreatedBy = creatorId;
            this.CreatedOn = DateTime.UtcNow;

            this.ProjectId = projectId;
            this.BoardId = boardId;
            this.Title = title;
            this.Description = description;
        }

        public Guid ProjectId { get; private set; }

        public Guid BoardId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }
        public DateTime CreatedOn { get; private set; }

        public DateTime UpdatedOn { get; private set; }

        public List<string> AssignedTo { get; private set; }

        public string CreatedBy { get; private set; }

        ICollection<IDomainEvent> IEventEmitter.Events { get; set; }

        public void UpdateContent(string title, string description)
        {
            this.Title = title;
            this.Description = description;
            this.UpdatedOn = DateTime.UtcNow;
        }

        public void UpdateAssigners(List<string> userIds)
        {
            this.AssignedTo = userIds;
            this.UpdatedOn = DateTime.UtcNow;
        }

        public void MoveToBoard(Guid newBoardId, List<Guid> newBoardStoriesPositions)
        {
            ((IEventEmitter)this).Events.Add(new StoryMovedEvent
            {
                StoryId = Id,
                NewBoardId = newBoardId,
                OldBoardId = this.BoardId,
                NewBoardStoriesPositions = newBoardStoriesPositions
            });
            this.BoardId = newBoardId;
        }

        public void MarkAsRemoved()
        {
            ((IEventEmitter)this).Events.Add(new StoryRemovedEvent
            {
                StoryId = Id,
                BoardId = this.BoardId
            });
        }
    }
}
