﻿using KanPan.Common;
using KanPan.Data.Stories;
using KanPan.Data.User;
using KanPan.Data.User.DataObjects;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanPan.EmailManager
{
    public class EmailSender
    {
        private readonly IStoryRepository _storyRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IOptions<Settings> _settings;
        private MailboxAddress _from;
        private readonly static string _subject = "Your awaiting tasks";

        public EmailSender()
        {
        }

        public EmailSender(
            IStoryRepository storyRepository,
            IAccountRepository accountRepository,
            IOptions<Settings> settings) : this()
        {
            _storyRepository = storyRepository;
            _accountRepository = accountRepository;
            _settings = settings;
            _from = new MailboxAddress(_settings.Value.EmailSenderName, _settings.Value.EmailSender);
        }

        public async Task SendMails()
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 587, false);
                    client.Authenticate(_settings.Value.EmailSender, _settings.Value.EmailSenderPass);

                    //get all stories which have been assigned to someone
                    var allStories = await _storyRepository.GetAllAssignedStories();
                    if (allStories == null || allStories.Count == 0) return;

                    //get users assignes to these stories
                    var allAssigners = allStories.SelectMany(s => s.AssignedTo).Distinct().ToList();
                    var allAssignedUsers = await _accountRepository.Get(allAssigners);

                    foreach (var assignedUser in allAssignedUsers)
                    {
                        var userStories = allStories.Where(s => s.AssignedTo.Contains(assignedUser.GoogleId))
                            .Select(s => s.Title)
                            .ToList();

                        var mailMessage = BuildMessage(assignedUser, userStories);
                        client.Send(mailMessage);
                    }

                    client.Disconnect(true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private MimeMessage BuildMessage(AccountDo assignedUser, List<string> storiesTitles)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(_from);
            mailMessage.Subject = _subject;
            mailMessage.To.Add(new MailboxAddress(string.Format("{0} {1}", assignedUser.FirstName, assignedUser.LastName), assignedUser.Email));
            mailMessage.Body = new TextPart("html")
            {
                Text = BuildMessageContent(assignedUser.FirstName, storiesTitles)
            };

            return mailMessage;
        }

        private string BuildMessageContent(string name, List<string> storiesTitles)
        {
            return string.Format(@"Hey {0},<br/><br/>

                                  These are the stories assigned to you:<br/><br/>

                                  <b><i>{1}<i></b><br/>

                                  <br/> Have a nice week!", name, string.Join("<br/>", storiesTitles));
        }
    }
}
