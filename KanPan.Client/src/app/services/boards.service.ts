import { AuthService } from './auth.service';

export class BoardsService extends AuthService {
    public getProjectBoards(projectId: any) {
        return this.authGet(`api/boards/${projectId}`);
    }

    public createBoard(projectId: any, title: string) {
        return this.authPost(`api/boards/${projectId}/new`, { Title: title });
    }

    public editBoard(projectId: any, boardId: any, newTitle: string) {
        return this.authPost(`api/boards/${projectId}/update/${boardId}`, { Title: newTitle });
    }

    public deleteBoard(projectId: any, id: any) {
        return this.authPost(`api/boards/${projectId}/delete/${id}`);
    }

    public saveBoardStoriesOrder(projectId: any, boardId: any, storyIdToPosition) {
        return this.authPost(`api/boards/${projectId}/${boardId}/storiesPositions`, storyIdToPosition);
    }
}