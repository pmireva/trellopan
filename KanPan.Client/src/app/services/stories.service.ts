import { AuthService } from './auth.service';

export class StoriesService extends AuthService {
    public create(projectId: any, boardId: any, story: any) {
        return this.authPost(`api/stories/${projectId}/${boardId}/new`, story);
    }

    public edit(projectId: any, boardId: any, storyId: any, story: any) {
        return this.authPost(`api/stories/${projectId}/${boardId}/update/${storyId}`, story);
    }

    public editAssigners(projectId: any, boardId: any, storyId: any, assigners: any[]) {
        return this.authPost(`api/stories/${projectId}/${boardId}/update/${storyId}/assigners`, { Assigners: assigners });
    }

    public move(projectId: any, boardId: any, storyId: any, storyIdToPosition: any[]) {
        return this.authPost(`api/stories/${projectId}/${boardId}/update/${storyId}/move`, storyIdToPosition);
    }

    public delete(projectId: any, boardId: any, id: any) {
        return this.authPost(`api/stories/${projectId}/${boardId}/delete/${id}`);
    }
}