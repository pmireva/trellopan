import { AuthService } from './auth.service';

export class ProjectsService extends AuthService {

    public getUserProjects() {
        return this.authGet('/api/projects');
    }

    public getUserProject(projectId: any) {
        return this.authGet(`api/projects/${projectId}`);
    }

    public createProject(title: string) {
        return this.authPost('api/projects/new', { Title: title });
    }
    public editProject(id: any, project: any) {
        return this.authPost(`api/projects/${id}`, project);
    }

    public deleteProject(id: any) {
        return this.authPost(`api/projects/delete/${id}`);
    }

    public getEditors(projectId: any) {
        return this.authGet(`api/projects/${projectId}/editors`);
    }

    public addEditor(projectId: any, editorId: any) {
        return this.authPost(`api/projects/${projectId}/editors/add/${editorId}`);
    }

    public removeEditor(projectId: any, editorId: any) {
        return this.authPost(`api/projects/${projectId}/editors/remove/${editorId}`);
    }

    public saveBoardsOrder(id: any, boardIdToPosition: any) {
        return this.authPost(`api/projects/boardsPositions/${id}`, boardIdToPosition);
    }
}