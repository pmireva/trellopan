import { Injectable } from "@angular/core";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

import { Router, CanActivate } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthService implements CanActivate {
    private tokenKey = "token";
    public currentUser: any;
    constructor(private http: Http, private router: Router) { }

    public canActivate() {
        if (this.checkLogin()) {
            return true;
        } else {
            this.router.navigate(['home']);
            return false;
        }
    }

    public setToken(accessToken: string, expiresAt: Date) {
        var tokenData = {
            accessToken: accessToken,
            validTo: expiresAt
        };
        localStorage.setItem(this.tokenKey, JSON.stringify(tokenData));
    }

    public authGet(url) {
        let headers = this.initAuthHeaders();
        let options = new RequestOptions({ headers: headers });
        return this.http.get(url, options).map(
            response => response.json()
        ).catch(this.handleError);
    }

    public checkLogin(): boolean {
        return this.getLocalToken() != null;
    }

    public authPost(url: string, body?: any) {
        let headers = this.initAuthHeaders();
        return this.http.post(url, body, { headers: headers })
            .map(response => {
                var contentType = response.headers.get('content-type');
                if (contentType && contentType.indexOf('application/json') > -1) {
                    return response.json();
                } else {
                    return '';
                }
            })
            .catch(this.handleError);
    }

    private getLocalToken(): string {
        let tokenData = JSON.parse(localStorage.getItem(this.tokenKey));
        if (tokenData != null && new Date(tokenData.validTo) >= new Date()) {
            return tokenData.accessToken;
        }

        return;
    }

    private initAuthHeaders(): Headers {
        let token = this.getLocalToken();
        if (token == null) { throw 'No token'; }

        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append("Authorization", "Bearer " + token);
        return headers;
    }

    private handleError(error: any) {
        if (error.status === 401) {
            this.router.navigate(["home"]);
        } else {
            let errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : 'Server error';
            console.error(errMsg);
            return Observable.throw(errMsg);
        }
    }
}
