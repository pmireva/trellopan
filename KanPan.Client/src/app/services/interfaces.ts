﻿/// <reference types="signalr" />

export interface NotificationSignalR extends SignalR {
    broadcaster: NotificationProxy;
}

export interface NotificationProxy {
    client: NotificationClient;
    server: NotificationServer;
}

export interface NotificationClient {
    setConnectionId: (id: string) => void;
    addNewBoard: (board: any) => void;
    updateBoard: (board: any) => void;
    deleteBoard: (boardId: any) => void;

    addNewStory: (story: any) => void;
    updateStory: (story: any) => void;
    deleteStory: (storyId: any) => void;
}

export interface NotificationServer {
    subscribe(boardId: any): void;
    unsubscribe(boardId: any): void;
}

export enum SignalRConnectionStatus {
    Connected = 1,
    Disconnected = 2,
    Error = 3
}