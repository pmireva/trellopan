import { Injectable } from "@angular/core";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import * as $ from 'jquery';

import { NotificationSignalR, NotificationProxy, NotificationClient, NotificationServer, SignalRConnectionStatus } from './interfaces';

@Injectable()
export class NotificationsService {
    currentState = SignalRConnectionStatus.Disconnected;
    connectionState: Observable<SignalRConnectionStatus>;

    setConnectionId: Observable<string>;
    addNewBoard: Observable<any>;
    updateBoard: Observable<any>;
    deleteBoard: Observable<any>;

    addNewStory: Observable<any>;
    updateStory: Observable<any>;
    deleteStory: Observable<any>;

    private connectionStateSubject = new Subject<SignalRConnectionStatus>();

    private setConnectionIdSubject = new Subject<string>();
    private addBoardSubject = new Subject<any>();
    private updateBoardSubject = new Subject<any>();
    private deleteBoardSubject = new Subject<any>();

    private addStorySubject = new Subject<any>();
    private updateStorySubject = new Subject<any>();
    private deleteStorySubject = new Subject<any>();

    private server: NotificationServer;

    constructor(private http: Http) {
        this.connectionState = this.connectionStateSubject.asObservable();

        this.setConnectionId = this.setConnectionIdSubject.asObservable();

        this.addNewBoard = this.addBoardSubject.asObservable();
        this.updateBoard = this.updateBoardSubject.asObservable();
        this.deleteBoard = this.deleteBoardSubject.asObservable();

        this.addNewStory = this.addStorySubject.asObservable();
        this.updateStory = this.updateStorySubject.asObservable();
        this.deleteStory = this.deleteStorySubject.asObservable();
     }

    start(debug: boolean): Observable<SignalRConnectionStatus> {

        $.connection.hub.logging = debug;

        let connection = <NotificationSignalR>$.connection;
        // reference signalR hub named 'broadcaster'
        let boardHub = connection.broadcaster;
        this.server = boardHub.server;
        // setConnectionId method called by server
        boardHub.client.setConnectionId = id => this.onSetConnectionId(id);

        // boards methods called by server
        boardHub.client.addNewBoard = board => this.onAddBoard(board);
        boardHub.client.updateBoard = board => this.onUpdateBoard(board);
        boardHub.client.deleteBoard = boardId => this.onDeleteBoard(boardId);

        boardHub.client.addNewStory = story => this.onAddStory(story);
        boardHub.client.updateStory = story => this.onUpdateStory(story);
        boardHub.client.deleteStory = storyId => this.onDeleteStory(storyId);

        // start the connection
        $.connection.hub.start()
            .done(response => this.setConnectionState(SignalRConnectionStatus.Connected))
            .fail(error => this.connectionStateSubject.error(error));

        return this.connectionState;
    }

    private setConnectionState(connectionState: SignalRConnectionStatus) {
        console.log('connection state changed to: ' + connectionState);
        this.currentState = connectionState;
        this.connectionStateSubject.next(connectionState);
    }

    // Client side methods
    private onSetConnectionId(id: string) {
        this.setConnectionIdSubject.next(id);
    }

    //board related
    private onAddBoard(board: any) {
        this.addBoardSubject.next(board);
    }

    private onUpdateBoard(board: any) {
        this.updateBoardSubject.next(board);
    }

    private onDeleteBoard(board: any) {
        this.deleteBoardSubject.next(board);
    }

    //story related
    private onAddStory(story: any) {
        this.addStorySubject.next(story);
    }

    private onUpdateStory(story: any) {
        this.updateStorySubject.next(story);
    }

    private onDeleteStory(story: any) {
        this.deleteStorySubject.next(story);
    }

    // Server side methods
    public subscribeToProject(projectId: any) {
        this.server.subscribe(projectId);
    }

    public unsubscribeFromProject(projectId: any) {
        this.server.unsubscribe(projectId);
    }
}