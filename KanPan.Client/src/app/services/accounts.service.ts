import { AuthService } from './auth.service';

export class AccountsService extends AuthService {
    public getAccount() {
        return this.authGet('/api/accounts');
    }

    public register() {
        return this.authPost('api/accounts/new', {});
    }

    public searchEditors(projectId: any, keyword: string) {
        return this.authGet(`api/accounts/${projectId}/search?term=${keyword}`);
    }
}