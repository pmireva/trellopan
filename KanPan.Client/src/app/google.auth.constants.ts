export class GoogleAuthConstants {
    private CLIENT_ID: string = '64608429341-ap8dt8snj1ulpph4rua0kjpvojoqnijh.apps.googleusercontent.com';

    public getClientId() : string {
        return this.CLIENT_ID;
    };
}