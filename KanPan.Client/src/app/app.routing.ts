import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './user/login.component';
import { AuthService } from './services/auth.service';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'about', component: AboutComponent },
    { path: 'user', loadChildren: 'app/user/user.module#UserModule' },
    { path: 'home', component: HomeComponent }
];


export const routing = RouterModule.forRoot(appRoutes);
