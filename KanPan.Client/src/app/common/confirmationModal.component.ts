﻿import { Component, Input, ViewChild, ElementRef, Inject, EventEmitter, Output } from '@angular/core';
import { NguiPopupComponent, NguiMessagePopupComponent } from "@ngui/popup";

@Component({
    moduleId: module.id,
    selector: 'confirmation-modal',
    template: `<div  class="confirmation-popup"><ngui-popup #popup></ngui-popup></div>`
})
export class ConfirmationModalComponent {
    @Input() title: string;
    @Input() mesage: string;
    @Output() onConfirm: EventEmitter<any> = new EventEmitter();
    @ViewChild(NguiPopupComponent) popup: NguiPopupComponent;

    constructor() {
    }

    openPopup(confirmationParams) {
        this.popup.open(NguiMessagePopupComponent, {
            classNames: 'small',
            title: this.title,
            buttons: {
                OK: () => {
                    this.onConfirm.emit(confirmationParams);
                    this.popup.close();
                },
                CANCEL: () => {
                    this.popup.close();
                }
            }
        });
    }
}