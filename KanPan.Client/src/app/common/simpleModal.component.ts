﻿import { Component, Input, ViewChild, ElementRef, Inject, EventEmitter, Output } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'simple-modal',
    template: `
      <div id="{{elementId}}" #modalcontainer class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
              <h4 class="modal-title">{{title}}</h4>
            </div>
            <div class="modal-body">
              <ng-content></ng-content>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" (click)="save()" data-dismiss="modal">Save</button>
            </div>
          </div>
        </div>
      </div>
      `,
      styles: [`
        .modal-body { background-color: #dae2e2; color: black; }
        .modal-header { background-color: #2fafa3; }
        .modal-footer { background-color: #dae2e2; }
      `]
})
export class SimpleModalComponent {
    @Input() title: string;
    @Input() elementId: string;
    @Output() onSave: EventEmitter<any> = new EventEmitter();
    @ViewChild('modalcontainer') containerEl: ElementRef;

    constructor() {}

    close() {
        $(this.containerEl.nativeElement).modal('hide');
    }

    save() {
        this.onSave.emit();
    }
}