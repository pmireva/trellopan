import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './user/login.component';

import { routing } from './app.routing';
import { AuthService } from "./services/auth.service";
import { NotificationsService } from "./services/notifications.service";
import { AccountsService } from "./services/accounts.service";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        routing
    ],
    providers: [
        AuthService,
        AccountsService,
        NotificationsService
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        AboutComponent,
        LoginComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
