import { Component, OnInit } from '@angular/core';
import { AccountsService } from "../services/accounts.service";
import { AuthService } from "../services/auth.service";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit {
    account: any;
    isLoggedIn: boolean = false;

    constructor(
        private accountsService: AccountsService,
        private authService: AuthService) { }

    ngOnInit() {
        let self = this;
        self.isLoggedIn = this.authService.checkLogin();
        if (self.isLoggedIn) {
            self.accountsService.getAccount().subscribe((response) => {
                self.account = response;
            });
        }
    }
}
