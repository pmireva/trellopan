import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { NotificationsService } from './services/notifications.service';
import { GoogleAuthConstants } from './google.auth.constants';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styles: [`
        main { padding: 5px 20px }
        ul { font-size: 16px; }
    `]
})
export class AppComponent implements OnInit {
    constructor(
        private authService: AuthService,
        private router: Router,
        private notificationsService: NotificationsService) { }

    public logout() {
        localStorage.clear();
        gapi.auth2.getAuthInstance().signOut();
        this.router.navigate(['home']);
    }

    ngOnInit() {
        var self = this;

        gapi.load('auth2', () => {
            gapi.auth2.init({
                client_id: new GoogleAuthConstants().getClientId()
            });
        });

        $.getScript(document.baseURI + "signalr/hubs").done(function () {
            self.notificationsService.start(true).subscribe(
                null,
                error => console.log('Error on init: ' + error));
        });

        if (!self.authService.checkLogin()) {
            self.router.navigate(['home']);
        } else {
            self.router.navigate(['user/projects']);
        }
    }
}