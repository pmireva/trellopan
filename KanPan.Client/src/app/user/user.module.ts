import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { userRoutes } from './user.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ProjectsService } from "../services/projects.service";
import { BoardsService } from "../services/boards.service";
import { StoriesService } from "../services/stories.service";
import { AccountsService } from "../services/accounts.service";
import { UserComponent } from './user.component';
import { ProjectsComponent } from './project/projects.component';
import { ProjectComponent } from './project/project.component';
import { EditStoryComponent } from './story/edit.story.component';
import { SimpleModalComponent } from '../common/simpleModal.component';
import { BoardControlsComponent } from './board/board.controls.component';
import { CreateBoardComponent } from './board/create.board.component';
import { MembersComponent } from './projectMembers/members.component';
import { AddEditorComponent } from './projectMembers/add.editor.component';

import { DndModule } from 'ng2-dnd';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { NguiPopupModule } from '@ngui/popup';
import { ConfirmationModalComponent } from "../common/confirmationModal.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NguiAutoCompleteModule,
    NguiPopupModule,
    DndModule.forRoot(),
    RouterModule.forChild(userRoutes)
  ],
  declarations: [
    UserComponent,
    ProjectsComponent,
    ProjectComponent,
    EditStoryComponent,
    SimpleModalComponent,
    BoardControlsComponent,
    CreateBoardComponent,
    MembersComponent,
    AddEditorComponent,
    ConfirmationModalComponent
  ],
  providers: [
      ProjectsService,
      StoriesService,
      BoardsService,
      AccountsService
  ],
  bootstrap: [UserComponent]
})
export class UserModule { }
