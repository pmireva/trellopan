﻿import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'members.component.html',
    selector: 'members',
    styleUrls: ['members.component.css']
})

export class MembersComponent {
    @Input() projectId: any;
    @Input() editors: any[];

    constructor() {
    }
}
