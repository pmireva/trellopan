﻿import { Component, Input } from '@angular/core';
import { ProjectsService } from "../../services/projects.service";
import { AccountsService } from "../../services/accounts.service";

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from "rxjs/Observable";

@Component({
    moduleId: module.id,
    templateUrl: 'add.editor.component.html',
    selector: 'add-editor',
    styles: [`
        .add-editor { color:black !important; display: inline-block; }
        .add-editor button { height: 35px; }
        .add-editor input { height: 35px; width: 85%; }
    `]
})

export class AddEditorComponent {
    @Input() projectId: any;
    @Input() editors: any[];
    selectedEditor: any;

    constructor(
        private projectsService: ProjectsService,
        private accountsService: AccountsService) { }

    suggestedEmails = (keyword: string): Observable<any[]> => {
        return this.accountsService.searchEditors(this.projectId, keyword);
    }

    addSelectedEditor(editor) {
        if (this.selectedEditor) {
            this.projectsService.addEditor(this.projectId, this.selectedEditor.GoogleId)
                .subscribe((newEditor) => {
                    this.selectedEditor = '';
                    this.editors.push(newEditor);
                });
        }
    }
}
