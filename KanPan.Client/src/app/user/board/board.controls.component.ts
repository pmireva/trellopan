﻿import { Component, OnInit, Input } from '@angular/core';
import { BoardsService } from "../../services/boards.service";
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
    moduleId: module.id,
    templateUrl: 'board.controls.component.html',
    selector: 'board-controls',
    styles: [`
        .board-control input {height: 25px; }
    `]
})

export class BoardControlsComponent implements OnInit {
    @Input() projectId: any;
    @Input() board: any;

    editBoardId: any;
    editBoardForm: FormGroup;
    changedBoardTitle: FormControl;


    constructor(private boardsService: BoardsService) { }

    ngOnInit() {
        this.changedBoardTitle = new FormControl('');
        this.editBoardForm = new FormGroup({
            changedBoardTitle: this.changedBoardTitle
        });
    }

    public deleteBoard(boardId: any) {
        this.boardsService.deleteBoard(this.projectId, boardId).subscribe();
    }

    public editBoard(boardId: any, title: string) {
        this.changedBoardTitle.setValue(title);
        this.editBoardId = boardId;
    }

    public sendEditBoard() {
        var newTitle = this.editBoardForm.value.changedBoardTitle;
        this.boardsService.editBoard(this.projectId, this.editBoardId, newTitle).subscribe();
        this.editBoardId = '';
        this.editBoardForm.reset();
    }
}
