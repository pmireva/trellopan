﻿import { Component, OnInit, Input } from '@angular/core';
import { BoardsService } from "../../services/boards.service";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'create.board.component.html',
    selector: 'create-board',
    styles: [`
        .create-board {display: inline-block; margin-left: 15px; }
        .create-board .form-control { height: 35px; }
    `]
})

export class CreateBoardComponent implements OnInit {
    @Input() projectId: any;

    newBoardForm: FormGroup;
    newBoard: FormControl;


    constructor(private boardsService: BoardsService) { }

    ngOnInit() {
        this.newBoard = new FormControl('');
        this.newBoardForm = new FormGroup({
            newBoard: this.newBoard
        });
    }

    public addNewBoard(form: any) {
        this.boardsService.createBoard(this.projectId, form.newBoard)
            .subscribe();
    }
}
