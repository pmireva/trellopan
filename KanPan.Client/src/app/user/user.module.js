"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var user_routes_1 = require("./user.routes");
var forms_1 = require("@angular/forms");
var projects_service_1 = require("../services/projects.service");
var boards_service_1 = require("../services/boards.service");
var stories_service_1 = require("../services/stories.service");
var accounts_service_1 = require("../services/accounts.service");
var user_component_1 = require("./user.component");
var projects_component_1 = require("./project/projects.component");
var project_component_1 = require("./project/project.component");
var edit_story_component_1 = require("./story/edit.story.component");
var simpleModal_component_1 = require("../common/simpleModal.component");
var board_controls_component_1 = require("./board/board.controls.component");
var create_board_component_1 = require("./board/create.board.component");
var members_component_1 = require("./projectMembers/members.component");
var add_editor_component_1 = require("./projectMembers/add.editor.component");
var ng2_dnd_1 = require("ng2-dnd");
var auto_complete_1 = require("@ngui/auto-complete");
var popup_1 = require("@ngui/popup");
var confirmationModal_component_1 = require("../common/confirmationModal.component");
var UserModule = (function () {
    function UserModule() {
    }
    return UserModule;
}());
UserModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            auto_complete_1.NguiAutoCompleteModule,
            popup_1.NguiPopupModule,
            ng2_dnd_1.DndModule.forRoot(),
            router_1.RouterModule.forChild(user_routes_1.userRoutes)
        ],
        declarations: [
            user_component_1.UserComponent,
            projects_component_1.ProjectsComponent,
            project_component_1.ProjectComponent,
            edit_story_component_1.EditStoryComponent,
            simpleModal_component_1.SimpleModalComponent,
            board_controls_component_1.BoardControlsComponent,
            create_board_component_1.CreateBoardComponent,
            members_component_1.MembersComponent,
            add_editor_component_1.AddEditorComponent,
            confirmationModal_component_1.ConfirmationModalComponent
        ],
        providers: [
            projects_service_1.ProjectsService,
            stories_service_1.StoriesService,
            boards_service_1.BoardsService,
            accounts_service_1.AccountsService
        ],
        bootstrap: [user_component_1.UserComponent]
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map