import { AuthService } from '../services/auth.service';
import { UserComponent } from './user.component';
import { ProjectsComponent } from './project/projects.component';
import { ProjectComponent } from './project/project.component';

export const userRoutes = [{
    path: '',
    component: UserComponent,
    canActivate: [AuthService],
    children: [
        { path: 'projects', component: ProjectsComponent },
        { path: 'projects/:id', component: ProjectComponent }
    ]
}];