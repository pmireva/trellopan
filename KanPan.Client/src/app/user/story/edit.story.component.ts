﻿import { Component, OnInit, Input, Output, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from "rxjs/Observable";

import { StoriesService } from '../../services/stories.service';
import * as $ from 'jquery';

@Component({
    moduleId: module.id,
    templateUrl: 'edit.story.component.html',
    selector: 'edit-story',
    styles: [`
    .edit-story {
        font-size: 28px;
        color: #2fafa3;
    }
    .edit-story:hover {
        color: #284e4b;
    }
    .modal-body { background-color: #dae2e2; color: black; }
    .modal-header { background-color: #2fafa3; }
    .modal-footer { background-color: #dae2e2; }
    `]
})

export class EditStoryComponent implements OnChanges {
    @ViewChild('closeBtn') closeBtn: ElementRef;
    description: FormControl;
    title: FormControl;
    storyForm: FormGroup;
    @Input() elementId: any;
    @Input() boardId: any;
    @Input() projectId: any;
    @Input() story: any;
    editMode: boolean = false;
    isReadonly: boolean = false;

    constructor(private storiesService: StoriesService) {
        this.title = new FormControl('', Validators.required);
        this.description = new FormControl('', Validators.required);
        this.storyForm = new FormGroup({
            title: this.title,
            description: this.description
        });
    }

    save() {
        if (!this.storyForm.valid) {
            return;
        }
        var storyModel = {
            Title: this.storyForm.value.title,
            Description: this.storyForm.value.description
        };
        var operation;
        if (this.story && this.story.Id) {
            operation = this.storiesService.edit(this.projectId, this.boardId, this.story.Id, storyModel);
        } else {
            operation = this.storiesService.create(this.projectId, this.boardId, storyModel);
        }
        operation.subscribe();
        this.storyForm.reset();

        this.closeBtn.nativeElement.click();
    }

    ngOnChanges() {
        this.isReadonly = this.story && this.story.Id ? true : false;
        this.setStoryValues();
    }

    cancelEditMode() {
        this.editMode = false;
        this.setStoryValues();
    }

    setStoryValues() {
        if (this.story) {
            this.storyForm.setValue({
                title: this.story.Title,
                description: this.story.Description
            });
        }
    }

}
