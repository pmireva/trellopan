import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { AccountsService } from "../services/accounts.service";
import { GoogleAuthConstants } from "../google.auth.constants";

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    selector: 'login'
})

export class LoginComponent {
    private googleApiAuth2: any;

    constructor(
        private authService: AuthService,
        private accountsService: AccountsService,
        private router: Router) {

        let self = this;
        if (!gapi.auth2) {
            gapi.load('auth2', () => {
                gapi.auth2.init({
                    client_id: new GoogleAuthConstants().getClientId()
                }).then(() => {
                    self.googleApiAuth2 = gapi.auth2.getAuthInstance();
                });
            });
        } else {
            self.googleApiAuth2 = gapi.auth2.getAuthInstance();
        }
    }

    login() {
        let self = this;
        this.googleApiAuth2.signIn().then((user) => {
            var authResponse = user.getAuthResponse();
            self.authService.setToken(authResponse.id_token, new Date(authResponse.expires_at));

            self.accountsService.getAccount().subscribe((response) => {
                if (response) {
                    self.router.navigate(['user/projects']);
                } else {
                    self.accountsService.register().subscribe(() =>
                        this.router.navigate(['user/projects'])
                    );
                }
            });
        });
    }
}
