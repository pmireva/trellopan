import { Component, OnInit } from '@angular/core';
import { ProjectsService } from "../../services/projects.service";
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'projects.component.html',
    styleUrls: ['projects.component.css']
})

export class ProjectsComponent implements OnInit {

    projects: any[];
    editProjectId: any;
    deleteProjectId: any;
    newProjectForm: FormGroup;
    newProjectTitle: FormControl;
    editProjectForm: FormGroup;
    changedProjectTitle: FormControl;

    constructor(
        private projectsService: ProjectsService,
        private router: Router) { }

    ngOnInit() {
        this.newProjectTitle = new FormControl('');
        this.changedProjectTitle = new FormControl('');

        this.newProjectForm = new FormGroup({
            newProjectTitle: this.newProjectTitle
        });
        this.editProjectForm = new FormGroup({
            changedProjectTitle: this.changedProjectTitle
        });

        this.projectsService.getUserProjects().subscribe(
            projects => {
                this.projects = projects;
            });
    }

    public view(projectId) {
        this.router.navigate([`user/projects/${projectId}`]);
    }

    public editName(project, index) {
        this.editProjectForm.setValue({
            changedProjectTitle: project.Title
        });
        this.editProjectId = project.Id;
    }

    public cancelEditName() {
        this.editProjectId = '';
    }

    public deleteProject(project) {
        this.deleteProjectId = project.Id;
    }

    public cancelDelete() {
        this.deleteProjectId = '';
    }

    public delete(itemIndex) {
        this.projectsService.deleteProject(this.deleteProjectId).subscribe();
        this.projects.splice(itemIndex, 1);
        this.deleteProjectId = '';
    }

    public addNew(formValues) {
        this.projectsService.createProject(formValues.newProjectTitle).subscribe(
            projects => {
                this.newProjectForm.reset();
                this.projects = projects;
            });
    }

    public edit(project, changedProjectTitle) {
        project.Title = changedProjectTitle;
        this.projectsService.editProject(project.Id, project).subscribe();
        this.editProjectId = '';
    }
}
