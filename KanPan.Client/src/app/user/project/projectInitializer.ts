﻿import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ProjectsService } from "../../services/projects.service";
import { BoardsService } from "../../services/boards.service";
import { StoriesService } from "../../services/stories.service";
import { NotificationsService } from "../../services/notifications.service";
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SignalRConnectionStatus } from "../../services/interfaces";

export class ProjectInitializer implements OnDestroy {
    connectionId: string;
    project: any;
    boards: any[];
    editors: any[];

    addNewBoardSubscription: Subscription;
    updateBoardSubscription: Subscription;
    deleteBoardSubscription: Subscription;
    addNewStorySubscription: Subscription;
    updateStorySubscription: Subscription;
    deleteStorySubscription: Subscription;

    constructor(
        protected boardsService: BoardsService,
        protected notificationsService: NotificationsService,
        protected route: ActivatedRoute
    ) { }

    initialize() {
        let self = this;
        this.listenForConnection(() => {
            self.loadData();
            self.addSubscriptions();
        });
    }

    loadData() {
        this.route.params.forEach((params: Params) => {
            this.boardsService.getProjectBoards(params['id'])
                .subscribe(
                data => {
                    this.project = data.Project;
                    this.boards = data.Boards;
                    this.editors = data.Editors;
                    this.notificationsService.subscribeToProject(this.project.Id);
                });
        });
    }

    listenForConnection(callback) {
        let self = this;
        var execute = () => {
            self.notificationsService.setConnectionId.subscribe(id => { self.connectionId = id; });
            callback();
        };
        if (self.notificationsService.currentState !== SignalRConnectionStatus.Connected) {
            self.notificationsService.start(true).subscribe(() => {
                execute();
            });
        } else {
            execute();
        }
    }

    addSubscriptions() {
        this.addNewBoardSubscription = this.notificationsService.addNewBoard.subscribe(
            board => {
                var boardIndex = this.boards.findIndex(b => b.Id === board.Id);
                if (boardIndex === -1) {
                    this.boards.push(board);
                }
            }
        );

        this.updateBoardSubscription = this.notificationsService.updateBoard.subscribe(
            board => {
                var boardIndex = this.boards.findIndex(b => b.Id === board.Id);
                this.boards[boardIndex].Title = board.Title;
            }
        );

        this.deleteBoardSubscription = this.notificationsService.deleteBoard.subscribe(
            boardId => {
                var boardIndex = this.boards.findIndex(b => b.Id === boardId);
                if (boardIndex > -1) {
                    this.boards.splice(boardIndex, 1);
                }
            }
        );

        this.addNewStorySubscription = this.notificationsService.addNewStory.subscribe(
            story => {
                var boardIndex = this.boards.findIndex(b => b.Id === story.BoardId);
                if (boardIndex > -1) {
                    var storyIndex = this.boards[boardIndex].Stories.findIndex(b => b.Id === story.Id);
                    if (storyIndex === -1) {
                        this.boards[boardIndex].Stories.push(story);
                    }
                }
            }
        );

        this.updateStorySubscription = this.notificationsService.updateStory.subscribe(
            story => {
                var boardIndex = this.boards.findIndex(b => b.Id === story.BoardId);
                var storyIndex = this.boards[boardIndex].Stories.findIndex(b => b.Id === story.Id);
                this.boards[boardIndex].Stories[storyIndex] = story;
            }
        );

        this.deleteStorySubscription = this.notificationsService.deleteStory.subscribe(
            deletedStory => {
                var boardIndex = this.boards.findIndex(b => b.Id === deletedStory.BoardId);
                if (boardIndex > -1) {
                    var storyIndex = this.boards[boardIndex].Stories.findIndex(b => b.Id === deletedStory.Id);
                    if (storyIndex > -1) {
                        this.boards[boardIndex].Stories.splice(storyIndex, 1);
                    }
                }

            }
        );

    }

    removeSubscriptions() {
        this.addNewBoardSubscription.unsubscribe();
        this.updateBoardSubscription.unsubscribe();
        this.deleteBoardSubscription.unsubscribe();
        this.addNewStorySubscription.unsubscribe();
        this.updateStorySubscription.unsubscribe();
        this.deleteStorySubscription.unsubscribe();

        this.notificationsService.unsubscribeFromProject(this.project.Id);
    }

    ngOnDestroy(): void {
        this.removeSubscriptions();
    }
}
