﻿import { Component, ViewChild, OnInit } from '@angular/core';
import { ProjectsService } from "../../services/projects.service";
import { BoardsService } from "../../services/boards.service";
import { StoriesService } from "../../services/stories.service";
import { NotificationsService } from "../../services/notifications.service";
import { ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DndModule } from 'ng2-dnd';
import { Subscription } from 'rxjs/Subscription';
import { SignalRConnectionStatus } from "../../services/interfaces";
import { ConfirmationModalComponent } from "../../common/confirmationModal.component";
import { ProjectInitializer } from "./projectInitializer";

@Component({
    moduleId: module.id,
    templateUrl: 'project.component.html',
    styleUrls: ['project.component.css']
})

export class ProjectComponent extends ProjectInitializer implements OnInit {
    storyToEdit: any;

    @ViewChild('removeFromStoryModal') removeFromStoryModal: ConfirmationModalComponent;

    dragBoards: boolean = false;

    constructor(
        private projectsService: ProjectsService,
        private storiesService: StoriesService,
        boardsService: BoardsService,
        notificationsService: NotificationsService,
        route: ActivatedRoute
    ) {
        super(boardsService, notificationsService, route);
    }

    ngOnInit() {
        this.initialize();
    }

    openPopup(story: any, assignerId: string) {
        this.removeFromStoryModal.openPopup({ story: story, assignerId: assignerId });
    }

    removeEditor(params: any) {
        var story = params.story;
        var assignerId = params.assignerId;

        var index = story.Assigners.findIndex(a => a.GoogleId === assignerId);
        if (index > -1) {
            story.Assigners.splice(index, 1);
            this.storiesService.editAssigners(this.project.Id, story.BoardId, story.Id, story.Assigners.map(a => a.GoogleId)).subscribe();
        }
    }

    reorderBoards() {
        if (this.dragBoards) {
            var boardIdToPosition = this.boards.map(b => b.Id);
            this.projectsService.saveBoardsOrder(this.project.Id, boardIdToPosition).subscribe();
        }
    }

    deleteStory(boardId, storyId) {
        this.storiesService.delete(this.project.Id, boardId, storyId).subscribe();
    }

    dropInBoard(story, event, board) {
        var assignedUser = event.dragData;
        if (assignedUser) {
            this.memberAssigned(story, assignedUser);
        } else {
            this.moveStory(story, board);
        }
        return true;
    }

    memberAssigned(story, assignedUser) {
        if (!story.Assigners) {
            story.Assigners = [];
        }
        var alreadyAddedindex = story.Assigners.findIndex(b => b.GoogleId === assignedUser.GoogleId);
        if (alreadyAddedindex === -1) {
            story.Assigners.push(assignedUser);

            this.storiesService.editAssigners(this.project.Id, story.BoardId, story.Id, story.Assigners.map(a => a.GoogleId)).subscribe();
        }

        return true;
    }

    moveStory(story, board) {
        if (story.BoardId !== board.Id) {
            var storyIdToPosition = board.Stories.map(s => s.Id);
            return this.storiesService.move(this.project.Id, board.Id, story.Id, storyIdToPosition)
                .subscribe(() => {
                    story.BoardId = board.Id;
                });
        } else {
            var storyIdToPosition = board.Stories.map(s => s.Id);
            return this.boardsService.saveBoardStoriesOrder(this.project.Id, board.Id, storyIdToPosition)
                .subscribe();
        }
    }
}
