﻿using System.IO;

namespace KanPan.Client
{
    public static class ClientApp
    {
        public static string Location = Path.Combine(Directory.GetCurrentDirectory(), "KanPan.Client");
    }
}
