﻿"use strict";

const gulp = require("gulp");
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("tsconfig.json");
const gulpTslint = require('gulp-tslint');

const buildDirectory = "wwwroot/dist";
/**
 * Remove build directory.
 */
gulp.task('clean', (cb) => {
    return del([buildDirectory], cb);
});


/**
* Lint files
**/
gulp.task('lint', ['clean'], () => {
    return gulp.src('src/**/*.ts')
        .pipe(gulpTslint())
        .pipe(gulpTslint.report());
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", ['lint', 'clean'], () => {
    let tsResult = gulp.src("src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsProject());
    return tsResult.js
        .pipe(sourcemaps.write(".", { sourceRoot: '/src' }))
        .pipe(gulp.dest(buildDirectory));
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
function copyResources() {
    return gulp.src(["src/**/*", "!**/*.ts"])
        .pipe(gulp.dest(buildDirectory));
}
gulp.task("resources", () => {
    copyResources();
});

gulp.task("syncResources", ['compile'], () => {
    copyResources();
});


/**
 * Copy all required libraries into build directory.
 */
gulp.task("libstyles", ['libs'], () => {
    return gulp.src([
        'ng2f-bootstrap/dist/bootstrap.min.css',
        'ng2f-bootstrap/fonts/*',
        'ng2-dnd/style.css'
    ], { cwd: "node_modules/**" }) 
        .pipe(gulp.dest(buildDirectory + "/lib"));
});

gulp.task("libs", ['compile'], () => {
    return gulp.src([
        'core-js/client/shim.min.js',
        'systemjs/dist/system-polyfills.js',
        'systemjs/dist/system.src.js',
        'bootstrap/dist/js/bootstrap.js',
        'rxjs-system-bundle/Rx.system.js',
        'zone.js/dist/**',
        '@angular/**/bundles/**',
        'ng2-dnd/bundles/index.umd.js',
        'signalr/jquery.signalR.min.js',
        'jquery/dist/jquery.min.js',
        '@ngui/auto-complete/dist/auto-complete.umd.js',
        '@ngui/popup/dist/popup.umd.js'
    ], { cwd: "node_modules/**" })
        .pipe(gulp.dest(buildDirectory + "/lib"));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', function () {
    gulp.watch(["src/**/*.ts"], ['clean', 'lint', 'compile', 'syncResources']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
    gulp.watch(["src/**/*.html", "src/**/*.css"], ['resources']).on('change', function (e) {
        console.log('Resource file ' + e.path + ' has been changed. Updating.');
    });
});

/**
 * Build the project.
 */
gulp.task("build", ['clean', 'lint', 'compile', 'syncResources', 'libs', 'libstyles'], () => {
    console.log("Building the project ...");
});