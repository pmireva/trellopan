(function (global) {
    System.config({
        paths: {
            'npm:': 'dist/lib/'
        },
         bundles: {
            "npm:rxjs-system-bundle/Rx.system.js": [
              "rxjs",
              "rxjs/*",
              "rxjs/operator/*",
              "rxjs/observable/*",
              "rxjs/scheduler/*",
              "rxjs/symbol/*",
              "rxjs/add/operator/*",
              "rxjs/add/observable/*",
              "rxjs/util/*"
            ]
          },
        map: {
            // our app is within the app folder
            app: 'dist/app',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

            // other libraries
            'ng2-dnd': 'npm:ng2-dnd/bundles/index.umd.js',
            'signalr': 'npm:signalr/jquery.signalR.min.js',
            'jquery': 'npm:jquery/dist/jquery.min.js',
            '@ngui/auto-complete': 'npm:@ngui/auto-complete/dist/auto-complete.umd.js',
            '@ngui/popup': 'npm:@ngui/popup/dist/popup.umd.js'
        },
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            }
        },
        typescriptOptions: {
          "compilerOptions": {
            "module": "commonjs",
            "experimentalDecorators": true
          }
        }
    });
})(this);
